using DevExpress.AspNetCore;
using DevExpress.AspNetCore.Reporting;
using DevExpress.XtraReports.Web.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using TestJose.DataAccess;
using TestJose.Services;

namespace TestJose
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddDevExpressControls();
            //services.AddSession();
            //services.AddScoped<ReportStorageWebExtension, CustomReportStorageWebExtension>();
            //services
            //   .AddMvc()
            //   .AddNewtonsoftJson()
            //   .SetCompatibilityVersion(Microsoft.AspNetCore.Mvc.CompatibilityVersion.Version_3_0);

            services.AddControllersWithViews().AddRazorRuntimeCompilation();

            services.AddDbContext<TestJoseDbContext>(options => options
                .UseLazyLoadingProxies()
                .UseMySql(Configuration.GetConnectionString("TestJose")));

            services.AddDefaultIdentity<ApplicationUser>()
                    .AddRoles<ApplicationRole>()
                   .AddEntityFrameworkStores<TestJoseDbContext>();

            //services.ConfigureReportingServices(configurator => {
            //    configurator.ConfigureReportDesigner(designerConfigurator => {
            //        designerConfigurator.EnableCustomSql();
            //        designerConfigurator.RegisterDataSourceWizardConfigFileConnectionStringsProvider();
            //        designerConfigurator.RegisterDataSourceWizardJsonConnectionStorage<CustomDataSourceWizardJsonDataConnectionStorage>(true);
            //    });
            //    configurator.ConfigureWebDocumentViewer(viewerConfigurator => {
            //        viewerConfigurator.UseCachedReportSourceBuilder();
            //        viewerConfigurator.RegisterJsonDataConnectionProviderFactory<CustomJsonDataConnectionProviderFactory>();
            //    });
            //});

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, TestJoseDbContext dataContext, ILoggerFactory loggerFactory)
        {
            //var reportingLogger = loggerFactory.CreateLogger("DXReporting");
            //DevExpress.XtraReports.Web.ClientControls.LoggerService.Initialize((exception, message) => {
            //    var logMessage = $"[{DateTime.Now}]: Exception occurred. Message: '{message}'. Exception Details:\r\n{exception}";
            //    reportingLogger.LogError(logMessage);
            //});
            //DevExpress.XtraReports.Configuration.Settings.Default.UserDesignerOptions.DataBindingMode = DevExpress.XtraReports.UI.DataBindingMode.Expressions;

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            //app.UseDevExpressControls();
            //app.UseSession();
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Projects}/{action=Index}/{id?}");

                endpoints.MapAreaControllerRoute(
                   name: "areas",
                   "Areas",
                   pattern: "{area:exists}/{controller=Default}/{action=Index}/{id?}");

                endpoints.MapRazorPages();
            });

            //run migration update database
            dataContext.Database.Migrate();
        }
    }
}
