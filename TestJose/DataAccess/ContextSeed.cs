﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace TestJose.DataAccess
{
    public class ContextSeed
    {
        public static async Task SeedRolesAsync(UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
        {
            //Seed Roles
            await roleManager.CreateAsync(new ApplicationRole(Roles.Admin.ToString()));
            await roleManager.CreateAsync(new ApplicationRole(Roles.Basic.ToString()));
        }

        public static async Task SeedUsersAsync(UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
        {
            //Seed Default User
            var defaultAdminUser = new ApplicationUser
            {
                Id = Guid.NewGuid().ToString(),
                UserName = "admin",
                Email = "admin@gmail.com",
                EmailConfirmed = true,
                PhoneNumberConfirmed = true
            };
            if (userManager.Users.All(u => u.Id != defaultAdminUser.Id))
            {
                var user = await userManager.FindByEmailAsync(defaultAdminUser.Email);
                if (user == null)
                {
                    await userManager.CreateAsync(defaultAdminUser, "123Pa$$word.");
                    await userManager.AddToRoleAsync(defaultAdminUser, Roles.Admin.ToString());
                }

            }

            var defaultBasicUser = new ApplicationUser
            {
                Id = Guid.NewGuid().ToString(),
                UserName = "userbasic",
                Email = "user@gmail.com",
                EmailConfirmed = true,
                PhoneNumberConfirmed = true
            };
            if (userManager.Users.All(u => u.Id != defaultBasicUser.Id))
            {
                var user = await userManager.FindByEmailAsync(defaultBasicUser.Email);
                if (user == null)
                {
                    await userManager.CreateAsync(defaultBasicUser, "123Pa$$word.");
                    await userManager.AddToRoleAsync(defaultBasicUser, Roles.Basic.ToString());
                }
            }
        }
    }
}
