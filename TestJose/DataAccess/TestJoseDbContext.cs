﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace TestJose.DataAccess
{
    public class TestJoseDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string, IdentityUserClaim<string>,
        ApplicationUserRole, IdentityUserLogin<string>,
        IdentityRoleClaim<string>, IdentityUserToken<string>>
    {
        public DbSet<Project> Project { get; set; }
        public DbSet<CommentProject> Comment { get; set; }
        public DbSet<TypeProject> TypeProject { get; set; }
        public DbSet<Section> Section { get; set; }
        public DbSet<ProjectSection> ProjectSection { get; set; }
        public DbSet<ProjectSite> ProjectSite { get; set; }
        public DbSet<Site> Site { get; set; }


        public TestJoseDbContext()
        {
        }

        public TestJoseDbContext(DbContextOptions<TestJoseDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<ProjectSection>()
               .HasKey(ps => new { ps.ProjectId, ps.SectionId });
                    modelBuilder.Entity<ProjectSection>()
                        .HasOne(ps => ps.Project)
                        .WithMany(p => p.ProjectSections)
                        .HasForeignKey(ps => ps.ProjectId);
                    modelBuilder.Entity<ProjectSection>()
                        .HasOne(ps => ps.Section)
                        .WithMany(c => c.ProjectSections)
                        .HasForeignKey(ps => ps.SectionId);

            modelBuilder.Entity<ProjectSite>()
              .HasKey(ps => new { ps.ProjectId, ps.SiteId });
            modelBuilder.Entity<ProjectSite>()
                .HasOne(ps => ps.Project)
                .WithMany(p => p.ProjectSites)
                .HasForeignKey(ps => ps.ProjectId);
            modelBuilder.Entity<ProjectSite>()
                .HasOne(ps => ps.Site)
                .WithMany(c => c.ProjectSites)
                .HasForeignKey(ps => ps.SiteId);

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ApplicationUserRole>(userRole =>
            {
                userRole.HasKey(ur => new { ur.UserId, ur.RoleId });

                userRole.HasOne(ur => ur.Role)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.RoleId)
                    .IsRequired();

                userRole.HasOne(ur => ur.User)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.UserId)
                    .IsRequired();
            });
        }
    }
}
