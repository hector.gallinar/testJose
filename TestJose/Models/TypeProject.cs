﻿using System.Collections.Generic;

namespace TestJose.DataAccess
{
    public class TypeProject
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public bool Active { get; set; }

        public virtual ICollection<Section> Sections { get; set; }
    }
}
