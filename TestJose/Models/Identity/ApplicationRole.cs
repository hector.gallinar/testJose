﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace TestJose.DataAccess
{
    public class ApplicationRole : IdentityRole
    {
        public virtual ICollection<ApplicationUserRole> UserRoles { get; set; }

        public ApplicationRole() 
        {
        }

        public ApplicationRole(string roleName) : base(roleName)
        {
        }
    }
}
