﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TestJose.DataAccess
{
    public class ApplicationUser: IdentityUser<string>
    {
        public string Name { get; set; }

        [Display(Name = "Role")]
        public virtual ICollection<ApplicationUserRole> UserRoles { get; set; }
    }
}
