﻿using System.Collections.Generic;

namespace TestJose.DataAccess
{
    public class Section
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public bool Active { get; set; }

        public virtual ICollection<ProjectSection> ProjectSections { get; set; }
    }
}
