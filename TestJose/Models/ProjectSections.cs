﻿namespace TestJose.DataAccess
{
    public class ProjectSection
    {
        public int ProjectId { get; set; }

        public int SectionId { get; set; }

        public virtual Project Project { get; set; }

        public virtual Section Section { get; set; }
    }
}
