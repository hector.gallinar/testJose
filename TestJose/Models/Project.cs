﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestJose.DataAccess
{
    public class Project
    {
        public Project()
        {
            ProjectSections = new HashSet<ProjectSection>();
            ProjectSites = new HashSet<ProjectSite>();
        }

        public int ID { get; set; }

        [Display(Name = "Type")]
        public int TypeProjectID { get; set; }

        [Display(Name = "Type")]
        public virtual TypeProject TypeProject { get; set; }

        [Required]
        public string Name { get; set; }

        public int Year { get; set; }

        [Display(Name = "Sections")]
        [Required]
        public virtual ICollection<ProjectSection> ProjectSections { get; set; }

        [Display(Name = "Sites")]
        [Required]
        public virtual ICollection<ProjectSite> ProjectSites { get; set; }

        public virtual ICollection<CommentProject> Comments { get; set; }

        [NotMapped]
        public ICollection<int> SectionIds { get; set; }

        [NotMapped]
        public ICollection<int> SiteIds { get; set; }
    }
}
