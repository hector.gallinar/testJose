﻿namespace TestJose.DataAccess
{
    public class ProjectSite
    {
        public int ProjectId { get; set; }

        public int SiteId { get; set; }

        public virtual Project Project { get; set; }

        public virtual Site Site { get; set; }
    }
}
