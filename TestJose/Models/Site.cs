﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TestJose.DataAccess
{
    public class Site
    {
        public int ID{ get; set; }

        [Required]
        [StringLength(5)]
        public string Code { get; set; }

        public string Description { get; set; }

        public bool Active { get; set; }

        public virtual ICollection<ProjectSite> ProjectSites { get; set; }
    }
}
