﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TestJose.DataAccess
{
    public class CommentProject
    {
        public int ID { get; set; }

        [Display(Name = "Project")]
        public int ProjectID { get; set; }

        public virtual Project Project { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime Date { get; set; }

        [Required]
        public string Comment { get; set; }

        [Display(Name = "User")]
        public string UserID { get; set; }

        public virtual ApplicationUser User { get; set; }

        [Display(Name = "Section")]
        public int SectionID { get; set; }

        public virtual Section Section{ get; set; }
    }
}
