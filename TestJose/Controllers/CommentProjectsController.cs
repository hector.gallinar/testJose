﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TestJose.DataAccess;
using X.PagedList;

namespace TestJose.Controllers
{
    [Authorize(Roles = "Admin,Basic")]
    public class CommentProjectsController : Controller
    {
        private readonly TestJoseDbContext _context;

        public CommentProjectsController(TestJoseDbContext context)
        {
            _context = context;
        }

        // GET: CommentProjects
        public async Task<IActionResult> Index(int? projectId, int? page)
        {
            ViewData["ProjectID"] = projectId;
            var comments = await _context.Comment
                .Include(c => c.Project)
                .Include(c => c.User)
                .Where(x => x.ProjectID == projectId)
                .OrderByDescending(x => x.Date)
                .ToListAsync();

            return View(comments.ToPagedList(page ?? 1, 10));
        }

        public async Task<IActionResult> MyComments(int? page)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var comments = await _context.Comment
                .Include(c => c.Project)
                .Include(c => c.User)
                .Where(x => x.UserID == userId)
                .OrderByDescending(x => x.Date)
                .ToListAsync();

            return View(comments.ToPagedList(page ?? 1, 3));
        }

        // GET: CommentProjects/Create
        public async Task<IActionResult> Create(int projectId)
        {
            await LoadViewData(projectId, null);

            return View();
        }

        // POST: CommentProjects/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,ProjectID,Date,Comment,SectionID")] CommentProject commentProject)
        {
            if (ModelState.IsValid)
            {
                var user = User.FindFirstValue(ClaimTypes.NameIdentifier);
                commentProject.UserID = user;

                _context.Add(commentProject);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index), new { projectId = commentProject.ProjectID });
            }
            await LoadViewData(commentProject.ProjectID, commentProject.SectionID);

            return View(commentProject);
        }

        // GET: CommentProjects/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var commentProject = await _context.Comment.FindAsync(id);
            if (commentProject == null)
            {
                return NotFound();
            }
            await LoadViewData(commentProject.ProjectID, commentProject.SectionID);

            return View(commentProject);
        }

        // POST: CommentProjects/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,ProjectID,Date,Comment,SectionID,UserID")] CommentProject commentProject)
        {
            if (id != commentProject.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(commentProject);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CommentProjectExists(commentProject.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index), new { projectId = commentProject.ProjectID });
            }
            await LoadViewData(commentProject.ProjectID, commentProject.SectionID);

            return View(commentProject);
        }

        private async Task LoadViewData(int projectId, int? sectionId)
        {
            ViewData["ProjectID"] = projectId;
            var projectSections = await _context.ProjectSection.Where(x => x.ProjectId == projectId).ToListAsync();
            ViewData["SectionID"] = new SelectList(projectSections, "SectionId", "Section.Name", sectionId);
        }

        // GET: CommentProjects/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var commentProject = await _context.Comment
                .Include(c => c.Project)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (commentProject == null)
            {
                return NotFound();
            }

            return View(commentProject);
        }

        // POST: CommentProjects/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var commentProject = await _context.Comment.FindAsync(id);
            _context.Comment.Remove(commentProject);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index), new { projectId = commentProject.ProjectID});
        }

        // GET: Projects/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var comment = await _context.Comment
                .Include(p => p.Section)
                .Include(p => p.User)
                .Include(p => p.Project)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (comment == null)
            {
                return NotFound();
            }

            return View(comment);
        }

        private bool CommentProjectExists(int id)
        {
            return _context.Comment.Any(e => e.ID == id);
        }
    }
}
