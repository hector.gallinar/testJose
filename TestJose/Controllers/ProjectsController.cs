﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestJose.DataAccess;
using X.PagedList;

namespace TestJose.Controllers
{
    [Authorize(Roles = "Admin,Basic")]
    public class ProjectsController : Controller
    {
        private readonly TestJoseDbContext _context;

        public ProjectsController(TestJoseDbContext context)
        {
            _context = context;
        }

        // GET: Projects
        public async Task<IActionResult> Index(int? page, int? filterType, string filterName, string filterSite, int? filterYear)
        {
            var projects = _context.Project.Include(p => p.TypeProject).Include(x => x.ProjectSections).AsQueryable();
            ViewData["TypeProjects"] = new SelectList(_context.TypeProject.Where(x => x.Active), "ID", "Name");
            ViewData["Years"] = new SelectList(Enumerable.Range(DateTime.Now.AddYears(-2).Year, 10).Select(x => new { ID = x, Name = x }), "ID", "Name");

            if (filterType.HasValue)
            {
                projects = projects.Where(x => x.TypeProjectID == filterType);
            }
            if(!string.IsNullOrEmpty(filterName))
            {
                projects = projects.Where(x => x.Name.Contains(filterName.Trim()));
            }
            //if (!string.IsNullOrEmpty(filterSite))
            //{
            //    projects = projects.Where(x => x.Site.Contains(filterSite.Trim()));
            //}
            if (filterYear.HasValue)
            {
                projects = projects.Where(x => x.Year.Equals(filterYear));
            }

            var list = await projects.ToListAsync();

            return View(list.ToPagedList(page ?? 1, 10));
        }

        // GET: Projects/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            LoadViewData(null, new List<int>(), new List<int>());

            return View();
        }


        // POST: Projects/Create
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,TypeProjectID,Name,SiteIds,Year,SectionIds")] Project project)
        {
            if (ModelState.IsValid)
            {
                if(project.SectionIds == null)
                {
                    ModelState.AddModelError("ProjectSections", "The Sections is required");
                    LoadViewData(project, project.SectionIds, project.SiteIds);
                    return View();
                }

                if (project.SiteIds == null)
                {
                    ModelState.AddModelError("ProjectSite", "The Sites is required");
                    LoadViewData(project, project.SectionIds, project.SiteIds);
                    return View();
                }
                project.ProjectSections = project.SectionIds.Select(x => new ProjectSection { SectionId = x }).ToList();
                project.ProjectSites = project.SiteIds.Select(x => new ProjectSite { SiteId = x }).ToList();
                _context.Add(project);

                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            LoadViewData(project, project.SectionIds, project.SiteIds);

            return View(project);
        }

        // GET: Projects/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var project = await _context.Project.FindAsync(id);
            if (project == null)
            {
                return NotFound();
            }
            var sectionListSelected = project.ProjectSections.Select(x => x.SectionId).ToList();
            var siteListSelected = project.ProjectSites.Select(x => x.SiteId).ToList();
            LoadViewData(project, sectionListSelected, siteListSelected);

            return View(project);
        }

        // POST: Projects/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int id, [Bind("ID,TypeProjectID,Name,SiteIds,Year,SectionIds")] Project project)
        {
            if (id != project.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (project.SectionIds == null)
                    {
                        ModelState.AddModelError("ProjectSections", "The Sections is required");
                        LoadViewData(project, project.SectionIds, project.SiteIds);
                        return View();
                    }
                    if (project.SiteIds == null)
                    {
                        ModelState.AddModelError("ProjectSites", "The Sites is required");
                        LoadViewData(project, project.SectionIds, project.SiteIds);
                        return View();
                    }

                    var addedSectionList = await ManageProjectSection(project.ID, project.SectionIds);
                    var addedSiteList = await ManageProjectSite(project.ID, project.SiteIds);
                    project.ProjectSections = addedSectionList.Select(x => new ProjectSection { SectionId = x }).ToList();
                    project.ProjectSites = addedSiteList.Select(x => new ProjectSite { SiteId = x }).ToList();

                    _context.Update(project);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProjectExists(project.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            LoadViewData(project, project.SectionIds, project.SiteIds);

            return View(project);
        }

        private async Task<List<int>> ManageProjectSection(int projectId, ICollection<int> sectionIds)
        {
            var projectSections = await _context.ProjectSection.Where(x => x.ProjectId == projectId).ToListAsync();
            var removeList = new List<ProjectSection>();
            var addedList = new List<int>();

            if (sectionIds != null)
            {
                removeList = projectSections.Where(x => !sectionIds.Contains(x.SectionId)).ToList();
                addedList = sectionIds.Where(x => !projectSections.Select(x => x.SectionId).Contains(x)).ToList();
            }
            else
            {
                removeList = projectSections;
            }
            _context.ProjectSection.RemoveRange(removeList);

            await _context.SaveChangesAsync();

            return addedList;
        }

        private async Task<List<int>> ManageProjectSite(int projectId, ICollection<int> siteIds)
        {
            var projectSites = await _context.ProjectSite.Where(x => x.ProjectId == projectId).ToListAsync();
            var removeList = new List<ProjectSite>();
            var addedList = new List<int>();

            if (siteIds != null)
            {
                removeList = projectSites.Where(x => !siteIds.Contains(x.SiteId)).ToList();
                addedList = siteIds.Where(x => !projectSites.Select(x => x.SiteId).Contains(x)).ToList();
            }
            else
            {
                removeList = projectSites;
            }
            _context.ProjectSite.RemoveRange(removeList);

            await _context.SaveChangesAsync();

            return addedList;
        }

        // GET: Projects/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var project = await _context.Project
                .Include(p => p.TypeProject)
                .Include(p => p.ProjectSections)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (project == null)
            {
                return NotFound();
            }

            return View(project);
        }

        // GET: Projects/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var project = await _context.Project
                .Include(p => p.TypeProject)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (project == null)
            {
                return NotFound();
            }

            return View(project);
        }

        // POST: Projects/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var project = await _context.Project.FindAsync(id);
            _context.Project.Remove(project);
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }

        private void LoadViewData(Project project, ICollection<int> sectionListSelected, ICollection<int> siteListSelected)
        {
            ViewData["TypeProjectID"] = new SelectList(_context.TypeProject.Where(x => x.Active), "ID", "Name", project?.TypeProjectID);
            ViewData["Sections"] = new MultiSelectList(_context.Section.Where(x => x.Active), "ID", "Name", sectionListSelected);
            ViewData["Sites"] = new MultiSelectList(_context.Site.Where(x => x.Active), "ID", "Code", siteListSelected);
            ViewData["Years"] = new SelectList(Enumerable.Range(DateTime.Now.AddYears(-2).Year, 10).Select(x => new { ID = x, Name = x }), "ID", "Name", DateTime.Now.Year);
        }

        private bool ProjectExists(int id)
        {
            return _context.Project.Any(e => e.ID == id);
        }
    }
}
