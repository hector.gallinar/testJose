﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TestJose.DataAccess;

namespace TestJose.Controllers
{
    [Authorize(Roles = "Admin")]
    public class TypeProjectsController : Controller
    {
        private readonly TestJoseDbContext _context;

        public TypeProjectsController(TestJoseDbContext context)
        {
            _context = context;
        }

        // GET: TypeProjects
        public async Task<IActionResult> Index()
        {
            return View(await _context.TypeProject.ToListAsync());
        }

        // GET: TypeProjects/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var typeProject = await _context.TypeProject
                .FirstOrDefaultAsync(m => m.ID == id);
            if (typeProject == null)
            {
                return NotFound();
            }

            return View(typeProject);
        }

        // GET: TypeProjects/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TypeProjects/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,Active")] TypeProject typeProject)
        {
            if (ModelState.IsValid)
            {
                _context.Add(typeProject);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(typeProject);
        }

        // GET: TypeProjects/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var typeProject = await _context.TypeProject.FindAsync(id);
            if (typeProject == null)
            {
                return NotFound();
            }
            return View(typeProject);
        }

        // POST: TypeProjects/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,Active")] TypeProject typeProject)
        {
            if (id != typeProject.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(typeProject);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TypeProjectExists(typeProject.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(typeProject);
        }

        // GET: TypeProjects/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var typeProject = await _context.TypeProject
                .FirstOrDefaultAsync(m => m.ID == id);
            if (typeProject == null)
            {
                return NotFound();
            }

            return View(typeProject);
        }

        // POST: TypeProjects/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var typeProject = await _context.TypeProject.FindAsync(id);
            _context.TypeProject.Remove(typeProject);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TypeProjectExists(int id)
        {
            return _context.TypeProject.Any(e => e.ID == id);
        }
    }
}
